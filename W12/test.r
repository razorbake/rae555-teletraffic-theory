rm(list=ls())
L<-4:0
T<-c(0,4,17,18,27)
T2<-c(0,1,5,14,27)
plot(T,L,type = "s", col = "red", lwd=3, xlab = "Time", ylab = "Number of job in queue", main = "Handling of jobs in the system");
par(new=TRUE)
plot(T2,L,type = "s", col = "green", lwd=3)
