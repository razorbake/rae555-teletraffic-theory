% M/M/2:8
lambda=8;
mu=9;
rau=lambda/mu;
P0 = (1-rau)/(1-(rau.^9));

P=zeros(1,8);
for i = 1:8
    P(1,i) = (rau.^i) * P0;
end