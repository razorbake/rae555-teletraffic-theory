rm(list=ls())
T<-seq(0,5,by = 0.1)
Y<-exp(-2*T)
Y2<-exp(-10*T)
plot(T,Y, col = "red", lwd=3, xlab = "Time", ylab = "Chance", main = "Chance of having new job in the system");
par(new=TRUE)
plot(T,Y2, col = "green", xlab = "Time", ylab = "Chance",lwd =3)